package com.rms.productservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rms.productservice.dto.ProductDto;
import com.rms.productservice.repository.ProductRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProductServiceApplicationTests {
    @Autowired
    private MockMvc mockMvc ;

    @Autowired
    private ObjectMapper objectMapper ;

    @Autowired
    private ProductRepository productRepository ;
    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:latest"));

    static {
        mongoDBContainer.start();
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Test
    @Order(1)
    void shouldCreateProduct() throws Exception {
        ProductDto productDto = getProductRequestBody() ;
        String productDtoString = objectMapper.writeValueAsString(productDto) ;
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/api/v1/product")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(productDtoString))
                .andExpect(status().isCreated()) ;

        Assertions.assertEquals(1, productRepository.findAll().size());
    }

    private ProductDto getProductRequestBody() {
        return ProductDto
                .builder()
                .name("bike helmet")
                .description("MT Stinger 2")
                .price(BigDecimal.valueOf(6550))
                .build() ;
    }

    @Test
    @Order(2)
    void shouldRetrieveAllProducts() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/v1/product")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

}
