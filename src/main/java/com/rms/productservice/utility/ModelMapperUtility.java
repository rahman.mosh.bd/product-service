package com.rms.productservice.utility;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperUtility {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper() ;
    }
}
