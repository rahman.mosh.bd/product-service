package com.rms.productservice.service;

import com.rms.productservice.dto.ProductDto;
import com.rms.productservice.dto.ProductResponseDto;
import com.rms.productservice.model.Product;
import com.rms.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository productRepository ;
    private final ModelMapper modelMapper;
    public void createProduct(ProductDto productDto) {
        Product product = Product.builder().name(productDto.getName()).description(productDto.getDescription()).price(productDto.getPrice()).build();
        productRepository.save(product) ;
        log.info("Product {} is saved with ID: {}",product.getName(), product.getId());
    }

    public List<ProductResponseDto> getProducts() {
        List<Product> allProducts = productRepository.findAll() ;
        return allProducts.stream().map(product -> modelMapper.map(product,ProductResponseDto.class)).collect(Collectors.toList());
    }
}
