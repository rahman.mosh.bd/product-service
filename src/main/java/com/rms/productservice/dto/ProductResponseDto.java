package com.rms.productservice.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductResponseDto {
    private String id ;
    private String name ;
    private String description ;
    private BigDecimal price ;
}
